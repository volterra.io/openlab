### DNS Delegation

!!! tldr "Feature: DNS Delegation"
    - If you delegate a subdomain to Volterra, Volterra can automatically create a vanity DNS record for your application and [install an SSL certificate from LetsEncrypt](https://www.volterra.io/docs/how-to/app-networking/http-load-balancer?#automatic-certificate-generation){target=\_blank rel="noopener}.
    - Volterra will manage renewals of the SSL certificate on your behalf.

1. Navigate to **System** -> **Manage** -> **Networking** -> **Delegation**

    ![DNS Delegation](../assets/img/shared/shared_dns_delegation.png)

    !!! info "Cool Feature"
        You can delegate multiple domains to Volterra to have even more customization!
