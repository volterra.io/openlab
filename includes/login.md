### Log into the Volterra Console

Open the **Volterra Console** and log in with your supplied credentials:

![Volterra Login Screen](../assets/img/shared/shared_login.png)