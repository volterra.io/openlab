### kubectl or GUI?

!!! tldr "Feature: Workload Deployment"
    - Operators can either use native Kubernetes tools, such as [`kubectl`](https://kubernetes.io/docs/reference/kubectl/overview/){target=\_blank rel="noopener} or the Volterra console to deploy applications.

1. Click on **Services**
2. Click on `...` to the right of `{{service}}` and click **Edit**

    ![vK8s Service]({{images.service}})

3. Click the `+++` next to `spec:` and scroll through the output.

    ![vK8s Edit Service]({{images.edit_service}})

    !!! info "Cool Feature"
        You can use [Volterra annotations](https://www.volterra.io/docs/reference/vks-api-comp#resource-management-for-volterra-vk8s){target=\_blank rel="noopener} to control where an application is deployed.

The [`service`](https://kubernetes.io/docs/concepts/services-networking/service/){target=\_blank rel="noopener} spec displayed in this screen is the same spec you would use with native Kubernetes tools. Volterra offers a convenient GUI to manage an application without requiring Kubernetes tool set experience.

For additional information on what Kubernetes APIs we support with vK8s, please see the [documentation](https://www.volterra.io/docs/reference/vks-api-comp){target=\_blank rel="noopener}.