### Easy to manage Kubernetes across all Volterra RE's

!!! tldr "Feature: Volterra Virtual Kubernetes"
    - Easy to manage multi-cluster Kubernetes
    - Volterra allows for centrally managed Kubernetes clusters
    - All RE (Regional Edge) can be used as a [Virtual Site](https://www.volterra.io/docs/ves-concepts/volterra-site#concept-of-where){target=_blank rel="noopener} to deploy the application on all Volterra REs

1. Navigate to **System** -> **Sites** -> **Site Map**

    ![Regional Edge Site Map](../assets/img/shared/shared_sites.png)

    !!! Info
        Each blue square on the map is a Volterra Regional Edge (RE) where the application can be deployed

2. Navigate to **Application** -> **{{namespace}}** ***Namespace*** -> **Applications** -> **Virtual K8s** and click on **{{namespace}}**

    ![Virtual Kubernetes]({{images.vk8s}})

3. The first screen that you see is the Volterra vK8s Dashboard, which shows the current health of your virtual Kubernetes environment, including:
    - Running and failed pods

    - Resource usage

    - The status of every backing site for the virtual cluster

        !!! Info
            A virtual Kubernetes (vK8s) cluster has been created on every Volterra RE across the globe.

4. Additional information for each tab displayed at the top of the vK8s dashboard page can be found below:

=== "Workloads"
    [**Workloads**](https://www.volterra.io/docs/ves-concepts/dist-app-mgmt#workload-object-and-api){target=_blank rel="noopener} are a Volterra construct that makes it easier to deploy an application without having to use a YAML manifest and with a minimum of Kubernetes knowledge, and includes options such as load balancers and persistent storage in one screen.
=== "Deployments"
    [**Deployments**](https://kubernetes.io/docs/concepts/workloads/controllers/deployment/){target=\_blank rel="noopener} are a Kubernetes construct used for application deployments. Allows you to view and _create_ new deployments
=== "Stateful Sets"
    [**Stateful Sets**](https://kubernetes.io/docs/concepts/workloads/controllers/statefulset/){target=\_blank rel="noopener} are a Kubernetes construct used for _stateful_ application deployments. Allows you to view and _create_ new StatefulSets
=== "Jobs"
    [**Jobs**](https://kubernetes.io/docs/concepts/workloads/controllers/job/){target=\_blank rel="noopener} are a Kubernetes construct used for pods that need to run a specified number of times and should end successfully. Allows you to view and _create_ new Jobs
=== "PVCs"
    [**PVCs**](https://kubernetes.io/docs/concepts/storage/persistent-volumes/){target=\_blank rel="noopener} are a Kubernetes construct for persistent storage for applications that require non-ephemeral storage.

    !!! info "Cool Feature"
        In the first quarter of 2021, Volterra will be implementing the popular [OpenEBS](https://openebs.io/){target=\_blank rel="noopener} product for storage resilience and replication!

=== "Services"
    [**Services**](https://kubernetes.io/docs/concepts/services-networking/service/){target=\_blank rel="noopener} are a Kubernetes construct exposing applications externally. Volterra load balancers will point to this service to expose your application either internally or externally.
=== "Configmaps"
    [**Configmaps**](https://kubernetes.io/docs/concepts/configuration/configmap/){target=\_blank rel="noopener} are a Kubernetes construct that contains non encrypted configuration data that can be read by pods, instead of putting the configuration data in the application itself.
=== "Secrets"
    [**Secrets**](https://kubernetes.io/docs/concepts/configuration/secret/){target=\_blank rel="noopener} are a Kubernetes construct where you can storage more sensitive information such as passwords.

    !!! info "Cool Feature"
        Kubernetes secrets are not encrypted and depend on you to secure them from being accessed. [Volterra has a method of encrypting application secrets](https://www.volterra.io/docs/how-to/secrets-management/app-secrets-using-wingman){target=\_blank rel="noopener} to prevent unauthorized access.
=== "Replica Sets"
    [**Replica Sets**](https://kubernetes.io/docs/concepts/workloads/controllers/replicaset/){target=\_blank rel="noopener} are a Kubernetes construct that contains maintains a specific number of replicas of your application. Generally Deployments are used instead of Replica Sets.
=== "Pods"
    [**Pods**](https://kubernetes.io/docs/concepts/workloads/pods/){target=\_blank rel="noopener} is the most basic building block of Kubernetes applications; a group of one or more containers that contain your application and specifications on how it should run.
=== "Events"
    A timeline of events of the vK8s cluster.
=== "Audit Logs"
    An audit trail of user initiated actions that were performed on the vK8s cluster.
