FROM python:latest

COPY requirements.txt requirements.txt

RUN pip install -r requirements.txt

WORKDIR /docs

EXPOSE 8000

ENTRYPOINT ["mkdocs"]

CMD ["serve","--dev-addr=0.0.0.0:8000"]