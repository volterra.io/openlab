---
title: API Auto-Discovery
namespace: hipster-shop-adn
service: cartservice
images:
    vk8s: ../assets/img/api_discovery/api_vk8s.png
    service: ../assets/img/api_discovery/api_service.png
    edit_service: ../assets/img/api_discovery/api_edit_service.png
---
{% include 'abbreviations.md' %}

# API Auto-Discovery with the Hipster Shop app

## Goal

Demonstrate API auto-discovery, analysis and security using a cloud native web-based microservices application deployed on Volterra ADN

## Deployment Scenario

- Deploy [Hipster Shop](https://github.com/lightstep/hipster-shop){target=\_blank rel="noopener}, a 10-tier microservices application across 8 Volterra Regional Edge nodes
- Apply Artificial Intelligence (AI) and Machine Learning (ML)

## Demonstrated Features

- Virtual Kubernetes (vK8s) across multiple Volterra RE's
- HTTP load balancer with a custom domain and automatic certificate creation
- Multi-cluster service mesh with end to end observability
- Use AI/ML for API auto discovery, analysis and security

## Prerequisites

- Watch a [**short video**](https://drive.google.com/file/d/1ptMmepSHHNh8oh-o6iFuehPk-3AgQHbf/view?usp=sharing){target=\_blank rel="noopener} of this demo
- A [**Volterra console**](https://showroom.console.ves.volterra.io/){target=\_blank rel="noopener} login

## Demo

The section below contains short demonstrations for the features listed above.

{% include 'login.md' %}

{% include 'vk8s.md' %}

{% include 'kubectl_or_gui.md' %}

{% include 'dns_delegation.md' %}

### Multi-Cluster Service Mesh  

!!! tldr "Feature: Observability"
    - Demonstration of the Dashboard
    - Requests and Security Events

1. Make sure you are in the **Application** section
2. Select the **{{namespace}}** namespace
3. Click on **Mesh** -> **Service Mesh**

![Service Mesh](../assets/img/api_discovery/api_service_mesh.png)

### AI/ML for API auto-discovery, analysis and security

!!! tldr "Feature: Observability and Security"
    - Automatically discover API endpoints for distributed application services
    - Performs behavioral analysis

1. Make sure you are in the **Application** section
2. Select the **{{namespace}}** namespace
3. Click on **Mesh** -> **Service Mesh**
4. Click on **API Endpoints** on the top menu

![AI/ML](../assets/img/api_discovery/api_ai_ml.png)
