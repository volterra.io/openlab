---
title: WordPress on ADN
namespace: wordpress
service: ams9-ams
images:
    vk8s: ../assets/img/adn_wordpress/adn_vk8s.png
    service: ../assets/img/adn_wordpress/adn_service.png
    edit_service: ../assets/img/adn_wordpress/adn_edit_service.png
---
{% include 'abbreviations.md' %}

# WordPress on Volterra's Application Delivery Network (ADN)

## Goal

The Volterra ADN is comprised of Volterra-managed [Regional Edge (RE)](https://www.volterra.io/docs/ves-concepts/system-overview){target=\_blank rel="noopener} points of presence (POP) spread across the globe where you can host your application without the need of on-premise hardware or other cloud provided infrastructure.

Discover how Volterra's Application Delivery Network (ADN) makes it easy to deploy and protect a web application.

## Deployment Scenario

- Deploy a global WordPress and MySQL installation running on Volterra's Virtual Kubernetes (vK8s) on Volterra's ADN
- Provide end-to-end observability

## Demonstrated Features

- Virtual Kubernetes on Volterra's ADN across all regional edge (RE)
- Delegated domains name
- HTTP load balancer with a custom domain and automatic certificate creation
- End to end observability

## Prerequisites

- Watch a [**short video**](https://drive.google.com/file/d/13O38xlRJGqOE-2FSE9gx4zR-c5IsMpu4/view?usp=sharing){target=\_blank rel="noopener} of this demo
- A [**Volterra console**](https://showroom.console.ves.volterra.io/){target=\_blank rel="noopener} login

## Demo

The section below contains short demonstrations for the features listed above.

{% include 'login.md' %}

{% include 'vk8s.md' %}

{% include 'kubectl_or_gui.md' %}

{% include 'dns_delegation.md' %}

### End-to-End Observability

When an application is exposed through Volterra's platform, the Volterra console provides a considerable amount of visibility into application performance. Important metrics, such as end to end latency and application error rates are displayed in an easy to digest dashboard for at-a-glance troubleshooting.

1. Click on **Virtual Hosts** -> **Load Balancers** -> **HTTP Load Balancers**
2. Click on **wordpress-frontend**
3. Take a look at the metrics on this screen, including *End to end Latency*

## Additional Information

For additional information, please visit the following links:

[Distributed Application Management](https://www.volterra.io/docs/ves-concepts/dist-app-mgmt){target=\_blank rel="noopener}
