---
title: Introduction to Terraform with Volterra
---
{% include 'abbreviations.md' %}

# Introduction to working with Terraform and Volterra

## Goal

Learn how to use the Volterra [Terraform](https://www.terraform.io/intro/index.html){target=\_blank rel="noopener} provider with the Volterra API to create a namespace. 

This is the **first** of a series of Terraform related Volterra labs.

## Prerequisites

- Basic knowledge of Terraform will be extremely helpful, as this series of labs wis not intended to teach you Terraform/[HashiCorp Configuration Language](https://www.terraform.io/docs/language/index.html){target=\_blank rel="noopener}
- A working Terraform installation. Terraform binaries can be found on on [HashiCorp's website](https://www.terraform.io/downloads.html){target=\_blank rel="noopener}. Installation documentation can be found [here](https://learn.hashicorp.com/tutorials/terraform/install-cli){target=\_blank rel="noopener}
- A Volterra account with sufficient access rights

## Lesson One: Preparing to use the Volterra Terraform provider

Before you can use the Volterra Terraform provider you will need to generate an API certificate. The API Certificate is used to authenticate you whenever you make a Volterra API call.

1. Log into the VoltConsole using your tenant credentials and click on the **General** option in the namespace selector.
2. Expand **Personal Management** and click **My Credentials**
3. Click **Create credentials**
4. Give your new credentials a name, such as _terraform_
5. Specify a password for the credentials file and make sure you know what it is, you will need this later!
6. Give the certificate an expiration date

    !!! note
        Certificate expiration can be set to a maximum of 3 months. Once the certificate expires you will need to generate a new one.

7. Click **Download** and the certificate file will be downloaded to your computer

## Lesson Two: Creating your first Terraform plan

In this lesson we will create a basic Terraform configuration file that will become the basis of all future Volterra Terraform configuration files.

1. Please open your IDE/editor of choice and start a new file. Don't worry if you don't have anything fancy, even the most basic text editor will work.
2. The first part of the Terraform configuration defines the Volterra Terraform provider. The code below instructs Terraform that it needs to download the Volterra provider of a specific version:

    ``` tf
    terraform {
      required_providers {
        volterra = {
        source = "volterraedge/volterra"
        version = "0.0.5"
        }
      }
    }
    ```

    !!! warning
        The Volterra provider version may be different from the time of publishing this document and when you run this lab. It's a good idea to stay current. You can find the latest version on the [Terraform Registry](https://registry.terraform.io/providers/volterraedge/volterra/latest){target=\_blank rel="noopener}. When upgrading your Terraform provider you will need to change the version in `main.tf`, delete the hidden `.terraform.lock.hcl` file, and then run `terraform init` again.

3. The next part of the Terraform configuration file specifies the path to the credentials file that you downloaded earlier, as well as the Tenant URL it will be issuing API calls against. Please modify the highlighted lines for your environment.

    ``` tf hl_lines="2 3"
    provider "volterra" {
      api_p12_file     = "/path/to/api_credential.p12"
      url              = "https://<tenant_name>.console.ves.volterra.io/api"
    }
    ```

4. Save the file to a directory as `main.tf`

## Lesson Three: Creating a tenant with Volterra's Terraform module

Now that we have everything that we need to start using Volterra's Terraform provider, we can add some very simple Terraform configuration to create a Volterra namespace.

1. In the same directory, create a new file named `namespace.tf` in your editor/IDE of choice.
2. Add the following code to your file, which will use the [`volterra_namespace` resource](https://www.terraform.io/docs/language/resources/syntax.html){target=\_blank rel="noopener}  to create a new namespace named `foo`

    ``` tf
    resource "volterra_namespace" "namespace" {
      name = "foo"
    }
    ```

3. Save the file

## Lesson Four: Running your Terraform configuration

Now comes the moment we've all been waiting for - running the Terraform configuration.

1. Open a Terminal window (or whichever shell you are using)

2. In order to use the Volterra Terraform provider, you will need to specify an environmental variable named `VES_P12_PASSWORD` with your certificate's password that you set above. Setting an environmental variable differs between operating systems/shells, so please consult the documentation. For example, with most *NIX based distributions you can enter `export VES_P12_PASSWORD=<your credential password>` into your terminal.

    !!! note
        If the terminal window is closed, the environmental variable will no longer be available and you will need to set it again. There are more permanent methods of setting this depending on the operating system/shell you are running.

3. Initialize Terraform by typing `terraform init` and hitting ++enter++. Notice how it will automatically download the Volterra provider for you!

4. Type `terraform plan` and hit ++enter++. Running `terraform plan` is a good practice, especially in the beginning. It ensures that the Terraform configuration you've written will work prior to a run that actually makes changes. If you've followed this lab guide successfully it will output something similar to the following:

    ``` tf
    An execution plan has been generated and is shown below.
    Resource actions are indicated with the following symbols:
    + create

    Terraform will perform the following actions:

    # volterra_namespace.namespace will be created
    + resource "volterra_namespace" "namespace" {
        + id          = (known after apply)
        + name        = "foo"
        + tenant_name = (known after apply)
        + uid         = (known after apply)
        }

    Plan: 1 to add, 0 to change, 0 to destroy.
    ```

5. Now, let's run it! Enter `terraform apply` and hit ++enter++ When it asks **"Do you want to perform these actions?"**, enter `yes` and hit ++enter++.

6. Voila! Your new tenant should be created, let's go into the Volterra console and take a look!

## Lesson Five: Undo

Now that we've created a new namespace, let's delete it. We're not going to delete it manually - we're going to delete it with Terraform!

1. To run your operation in reverse, type `terraform destroy` and hit ++enter++.

2. When it asks **"Do you really want to destroy all resources?"**, enter `yes` and hit ++enter++.

3. Observe as your tenant is deleted automagically!

## Conclusion

Terraform is a very powerful tool that not only allows you to create objects, but also destroy them. Volterra exposes almost all objects through Terraform resources, and we hope that you join us for the next in the series where we explore other use cases.
