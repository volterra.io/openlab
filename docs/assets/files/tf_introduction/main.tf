terraform {
  required_providers {
    volterra = {
      source  = "volterraedge/volterra"
      version = ">=0.0.5"
    }
  }
}

provider "volterra" {
  api_p12_file = "/path/cert.console.ves.volterra.io.api-creds.p12"
  url          = "https://tenant.console.ves.volterra.io/api"
}