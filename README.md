[![pipeline status](https://gitlab.com/volterra.io/openlab/badges/master/pipeline.svg)](https://gitlab.com/volterra.io/openlab/-/commits/master)

# ![Volterra Logo](docs/assets/img/shared/shared_logo_big.png)
## OpenLabs

This repository is for Volterra demos.

Please contact sales@volterra.io for more information.

## Technology

This site uses markdown based files for documentation, which are built into a static site using [MkDocs](https://www.mkdocs.org/).

The docs theme is [Material for MkDocs](https://squidfunk.github.io/mkdocs-material/)

## Build/Test

Instead of using your own Python environment on your local machine, you can use the Dockerfile in this repository. When run, the Docker image will serve the documentation site on http://0.0.0.0:8000. When changes are made to the documentation will automatically refresh.

`make build` will build the image on your local machine

`make run` will run the image and provide a development environment

## Contributing

To contribute to the documentation, please create a branch and then submit a GitLab merge request.
