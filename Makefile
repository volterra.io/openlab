OPENLABS_VERSION = latest
RUN_PATH = $(shell pwd)

.PHONY: build run

build:
		docker build -t volterra/openlabs:${OPENLABS_VERSION} .

run:
		docker run --rm -v ${RUN_PATH}:/docs -p 8000:8000 --name="openlabs" volterra/openlabs:${OPENLABS_VERSION}